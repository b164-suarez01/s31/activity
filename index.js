const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./routes/taskRoute')

//Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect('mongodb+srv://bssuarez07:Crusaders07@cluster00.qysuq.mongodb.net/batch164_to-do?retryWrites=true&w=majority',
{
	useNewUrlParser:true,
	useUnifiedTopology:true
})

let db = mongoose.connection;
//connection error message
db.on("error", console.error.bind(console,"connection error")) 
;
//connection is successful message
db.once("open", ()=>console.log("We're connected to the cloud database"));

app.listen(port, ()=> console.log(`Server running at port ${port}`));
/*
Notes:
models > controllers > routes > index.js

*/

//Roue \
app.use("/tasks", taskRoute);
///http://localhost:3001/tasks/